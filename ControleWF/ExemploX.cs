﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Track{

public class Form1 : System.Windows.Forms.Form
{
    private System.Windows.Forms.TrackBar trackBar1;
    private System.Windows.Forms.TextBox textBox1;

    //[STAThread]
    //static void Main()
    //{
    //    Application.Run(new Form1());
    //}

    public Form1()
    {
        this.textBox1 = new System.Windows.Forms.TextBox();
        this.trackBar1 = new System.Windows.Forms.TrackBar();

        // TextBox for TrackBar.Value update.
        this.textBox1.Location = new System.Drawing.Point(240, 16);
        this.textBox1.Size = new System.Drawing.Size(48, 20);

        // Set up how the form should be displayed and add the controls to the form.
        this.ClientSize = new System.Drawing.Size(296, 62);
        this.Controls.AddRange(new System.Windows.Forms.Control[] { this.textBox1, this.trackBar1 });
        this.Text = "TrackBar Example";

        // Set up the TrackBar.
        this.trackBar1.Location = new System.Drawing.Point(8, 8);
        this.trackBar1.Size = new System.Drawing.Size(224, 45);
        this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);

        // The Maximum property sets the value of the track bar when
        // the slider is all the way to the right.
        trackBar1.Maximum = 30;

        // The TickFrequency property establishes how many positions
        // are between each tick-mark.
        trackBar1.TickFrequency = 5;

        // The LargeChange property sets how many positions to move
        // if the bar is clicked on either side of the slider.
        trackBar1.LargeChange = 3;

        // The SmallChange property sets how many positions to move
        // if the keyboard arrows are used to move the slider.
        trackBar1.SmallChange = 2;
    }


    private void trackBar1_Scroll(object sender, System.EventArgs e)
    {
        // Display the trackbar value in the text box.
        textBox1.Text = "" + trackBar1.Value;

    }

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.textBox2 = new System.Windows.Forms.TextBox();
        this.SuspendLayout();
        // 
        // textBox2
        // 
        this.textBox2.Location = new System.Drawing.Point(37, 39);
        this.textBox2.Name = "textBox2";
        this.textBox2.Size = new System.Drawing.Size(100, 20);
        this.textBox2.TabIndex = 0;
        // 
        // Form1
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(284, 262);
        this.Controls.Add(this.textBox2);
        this.Name = "Form1";
        this.Text = "Form1";
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBox2;

}

}