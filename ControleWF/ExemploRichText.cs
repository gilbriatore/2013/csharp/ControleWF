﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControleWF
{
    public partial class ExemploRichText : Form
    {

        //[STAThread]
        //static void Main()
        //{
        //    Application.EnableVisualStyles();
        //    Application.Run(new ExemploRichText());
        //}

        public ExemploRichText()
        {
            InitializeComponent();
            CreateMyRichTextBox();
        }

        public void CreateMyRichTextBox()
        {
            RichTextBox richTextBox1 = new RichTextBox();
            richTextBox1.Dock = DockStyle.Fill;

            //richTextBox1.Text = "ajklfjdlflasdf";
            richTextBox1.LoadFile("D:\\Google Drive\\Positivo\\UP\\C#\\Material\\Aula 14 - Linguagem III\\RTF\\Uma Criatura.rtf");
            richTextBox1.Find("Text", RichTextBoxFinds.MatchCase);

            richTextBox1.SelectionFont = new Font("Verdana", 12, FontStyle.Bold);
            richTextBox1.SelectionColor = Color.Red;

            richTextBox1.SaveFile("C:\\MyDocument.rtf", RichTextBoxStreamType.RichText);

            this.Controls.Add(richTextBox1);
        }
    }
}
