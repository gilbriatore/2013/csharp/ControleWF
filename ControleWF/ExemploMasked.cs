﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControleWF
{
    public partial class ExemploMasked : Form
    {

        //[STAThread]
        //static void Main()
        //{
        //    Application.EnableVisualStyles();
        //    //Application.SetCompatibleTextRenderingDefault(false);
        //    Application.Run(new Form1());
        //}

        private void Form1_Load(object sender, EventArgs e)
{
    maskedTextBox1.Mask = "00/00/0000";

    maskedTextBox1.MaskInputRejected += new MaskInputRejectedEventHandler(maskedTextBox1_MaskInputRejected);
    maskedTextBox1.KeyDown += new KeyEventHandler(maskedTextBox1_KeyDown);
}

void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
{
    if (maskedTextBox1.MaskFull)
    {
        toolTip1.ToolTipTitle = "Input Rejected - Too Much Data";
        toolTip1.Show("You cannot enter any more data into the date field. Delete some characters in order to insert more data.", maskedTextBox1, 0, -20, 5000);
    }
    else if (e.Position == maskedTextBox1.Mask.Length)
    {
        toolTip1.ToolTipTitle = "Input Rejected - End of Field";
        toolTip1.Show("You cannot add extra characters to the end of this date field.", maskedTextBox1, 0, -20, 5000);
    }
    else
    {
        toolTip1.ToolTipTitle = "Input Rejected";
        toolTip1.Show("You can only add numeric characters (0-9) into this date field.", maskedTextBox1, 0, -20, 5000);
    }
}

void maskedTextBox1_KeyDown(object sender, KeyEventArgs e)
{
    // The balloon tip is visible for five seconds; if the user types any data before it disappears, collapse it ourselves.
    toolTip1.Hide(maskedTextBox1);
}


        public ExemploMasked()
        {
            InitializeComponent();
        }



        private void Form1_Load_1(object sender, EventArgs e)
        {
            maskedTextBox1.Mask = "00/00/0000";
            maskedTextBox1.Text = "30/10/2013";
            maskedTextBox1.MaskInputRejected += new MaskInputRejectedEventHandler(maskedTextBox1_MaskInputRejected);
            maskedTextBox1.KeyDown += new KeyEventHandler(maskedTextBox1_KeyDown);
        }

        private void maskedTextBox1_MaskInputRejected_1(object sender, MaskInputRejectedEventArgs e)
        {
            if (maskedTextBox1.MaskFull)
            {
                toolTip1.ToolTipTitle = "Input Rejected - Too Much Data";
                toolTip1.Show("You cannot enter any more data into the date field. Delete some characters in order to insert more data.", maskedTextBox1, 0, -20, 5000);
            }
            else if (e.Position == maskedTextBox1.Mask.Length)
            {
                toolTip1.ToolTipTitle = "Input Rejected - End of Field";
                toolTip1.Show("You cannot add extra characters to the end of this date field.", maskedTextBox1, 0, -20, 5000);
            }
            else
            {
                toolTip1.ToolTipTitle = "Input Rejected";
                toolTip1.Show("You can only add numeric characters (0-9) into this date field.", maskedTextBox1, 0, -20, 5000);
            }
        }

        private void maskedTextBox1_KeyDown_1(object sender, KeyEventArgs e)
        {
            // The balloon tip is visible for five seconds; if the user types any data before it disappears, collapse it ourselves.
            toolTip1.Hide(maskedTextBox1);

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show(checkBox1.Text);
            MessageBox.Show("" + checkBox1.Checked);
        }

    }
}
