﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace xxx
{

    public class ExemploToolStrip2 : Form
    {
        private ToolStripContainer toolStripContainer1;
        private DateTimePicker dateTimePicker1;
        private ToolStrip toolStrip1;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new ExemploToolStrip2());
        }

        public ExemploToolStrip2()
        {
            Criar();

        }

        private void Criar()
        {
            toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            toolStrip1 = new System.Windows.Forms.ToolStrip();
            // Add items to the ToolStrip.
            toolStrip1.Items.Add("One");
            toolStrip1.Items.Add("Two");
            toolStrip1.Items.Add("Three");
            // Add the ToolStrip to the top panel of the ToolStripContainer.
            toolStripContainer1.TopToolStripPanel.Controls.Add(toolStrip1);
            // Add the ToolStripContainer to the form.
            Controls.Add(toolStripContainer1);

        }

        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(35, 31);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // ExemploToolStrip2
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.dateTimePicker1);
            this.Name = "ExemploToolStrip2";
            this.ResumeLayout(false);

        }
    }

}