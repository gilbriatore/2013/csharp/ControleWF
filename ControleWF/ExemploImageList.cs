﻿namespace ControleWF
{
    using System;
    using System.Drawing;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class ExemploImageList : System.Windows.Forms.Form
    {
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList listaDeImagens;
        private System.Windows.Forms.OpenFileDialog caixaDeDialogo;
        protected Graphics myGraphics;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private int currentImage = 0;

        public ExemploImageList()
        {
            InitializeComponent();
            listaDeImagens = new ImageList();

            // The default image size is 16 x 16, which sets up a larger
            // image size. 
            listaDeImagens.ImageSize = new Size(255, 255);
            listaDeImagens.TransparentColor = Color.White;

            // Assigns the graphics object to use in the draw options.
            myGraphics = Graphics.FromHwnd(panel1.Handle);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listaDeImagens = new System.Windows.Forms.ImageList(this.components);
            this.caixaDeDialogo = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Location = new System.Drawing.Point(16, 16);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(400, 95);
            this.listBox1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "label3";
            // 
            // btnAbrir
            // 
            this.btnAbrir.Location = new System.Drawing.Point(96, 128);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(104, 23);
            this.btnAbrir.TabIndex = 6;
            this.btnAbrir.Text = "Show Next Image";
            this.btnAbrir.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(208, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Remove Image";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(320, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Clear List";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(16, 128);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Open Image";
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(328, 232);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(336, 192);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // listaDeImagens
            // 
            this.listaDeImagens.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.listaDeImagens.ImageSize = new System.Drawing.Size(16, 16);
            this.listaDeImagens.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(8, 240);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(296, 184);
            this.panel1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(168, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(312, 40);
            this.label5.TabIndex = 0;
            this.label5.Text = "label5";
            // 
            // ExemploImageList
            // 
            this.ClientSize = new System.Drawing.Size(672, 461);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnAbrir);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listBox1);
            this.Name = "ExemploImageList";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        // Display the image.
        private void button1_Click(object sender, System.EventArgs e)
        {
            if (listaDeImagens.Images.Empty != true)
            {
                if (listaDeImagens.Images.Count - 1 > currentImage)
                {
                    currentImage++;
                }
                else
                {
                    currentImage = 0;
                }
                panel1.Refresh();

                // Draw the image in the panel.
                listaDeImagens.Draw(myGraphics, 10, 10, currentImage);

                // Show the image in the PictureBox.
                pictureBox1.Image = listaDeImagens.Images[currentImage];
                label3.Text = "Current image is " + currentImage;
                listBox1.SelectedIndex = currentImage;
                label5.Text = "Image is " + listBox1.Text;
            }
        }

        // Remove the image.
        private void button2_Click(object sender, System.EventArgs e)
        {
            listaDeImagens.Images.RemoveAt(listBox1.SelectedIndex);
            listBox1.Items.Remove(listBox1.SelectedItem);
        }

        // Clear all images.
        private void button3_Click(object sender, System.EventArgs e)
        {
            listaDeImagens.Images.Clear();
            listBox1.Items.Clear();
        }

        // Find an image.
        private void button4_Click(object sender, System.EventArgs e)
        {
            caixaDeDialogo.Multiselect = true;
            if (caixaDeDialogo.ShowDialog() == DialogResult.OK)
            {
                if (caixaDeDialogo.FileNames != null)
                {
                    for (int i = 0; i < caixaDeDialogo.FileNames.Length; i++)
                    {
                        addImage(caixaDeDialogo.FileNames[i]);
                    }
                }
                else
                    addImage(caixaDeDialogo.FileName);
            }

        }

        private void addImage(string imageToLoad)
        {
            if (imageToLoad != "")
            {
                listaDeImagens.Images.Add(Image.FromFile(imageToLoad));
                listBox1.BeginUpdate();
                listBox1.Items.Add(imageToLoad);
                listBox1.EndUpdate();
            }
        }
        //[STAThread]
        //public static void Main(string[] args)
        //{
        //    Application.Run(new ExemploImageList());
        //}
    }
}