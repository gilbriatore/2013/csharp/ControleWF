﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControleWF
{
    partial class ExemploLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExemploLabel));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.mediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "chicoanisio.jpg");
            this.imageList1.Images.SetKeyName(1, "fabiojunior.jpg");
            this.imageList1.Images.SetKeyName(2, "jimcarrey.jpg");
            this.imageList1.Images.SetKeyName(3, "josoares.jpg");
            this.imageList1.Images.SetKeyName(4, "michael.jpg");
            this.imageList1.Images.SetKeyName(5, "obama.jpg");
            this.imageList1.Images.SetKeyName(6, "paloci.jpg");
            this.imageList1.Images.SetKeyName(7, "putin.jpg");
            this.imageList1.Images.SetKeyName(8, "roberto.jpg");
            this.imageList1.Images.SetKeyName(9, "ronaldinho.jpg");
            this.imageList1.Images.SetKeyName(10, "ronaldo.jpg");
            this.imageList1.Images.SetKeyName(11, "serra.jpg");
            this.imageList1.Images.SetKeyName(12, "willsmith.jpg");
            this.imageList1.Images.SetKeyName(13, "xuxa.jpg");
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label1.ImageIndex = 9;
            this.label1.ImageList = this.imageList1;
            this.label1.Location = new System.Drawing.Point(38, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 153);
            this.label1.TabIndex = 0;
            this.label1.Text = "label2";
            // 
            // mediaPlayer
            // 
            this.mediaPlayer.Enabled = true;
            this.mediaPlayer.Location = new System.Drawing.Point(49, 65);
            this.mediaPlayer.Name = "mediaPlayer";
            this.mediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("mediaPlayer.OcxState")));
            this.mediaPlayer.Size = new System.Drawing.Size(391, 309);
            this.mediaPlayer.TabIndex = 1;
            this.mediaPlayer.Enter += new System.EventHandler(this.axWindowsMediaPlayer1_Enter);
            // 
            // ExemploLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 439);
            this.Controls.Add(this.mediaPlayer);
            this.Controls.Add(this.label1);
            this.Name = "ExemploLabel";
            this.Text = "ExemploLabel";
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private AxWMPLib.AxWindowsMediaPlayer mediaPlayer;
    }
}